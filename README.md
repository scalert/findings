# findings

This project contains findings about state of things in the industry when it comes to easy scaling web applications.

# tests

One of the projects that seem interesting is the following:

https://github.com/aws-samples/amazon-ecs-multi-region

### observations

#### binary availability

I had to look around how npm works. One of the instructions says to run `cdk bootstrap`, but that did not work as `cdk` was not available in my PATH.
I've resolved it by using `npx`, but all of this seems suboptimal. Docker image approach might be much better.

#### versions mismatch

The following is interesting as well:

```
mhuseinbasic:~/workspace/amazon-ecs-multi-region$ git log -n 1
commit 493607e45371e5121e58754aca557ebca641287c (HEAD -> main, origin/main, origin/HEAD)
Author: Abdel-Rahman Awad <halwsa@live.com>
Date:   Tue Oct 11 00:04:16 2022 +0200

    maxHealthyPercent to 100
mhuseinbasic:~/workspace/amazon-ecs-multi-region$ git pull
Already up to date.
mhuseinbasic:~/workspace/amazon-ecs-multi-region$ cdk/node_modules/aws-cdk/bin/cdk --version
2.37.1 (build f15dee0)
mhuseinbasic:~/workspace/amazon-ecs-multi-region$ npm view cdk version
2.87.0
```

So it seems that version which main branch on GitHub provides is older than the version available in npm registry.
It is unknown at the time of writing where is the newer version coming from.